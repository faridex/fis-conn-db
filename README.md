# Spring-Boot Camel QuickStart



### Building

Buildando 

    mvn clean install

### Running in OpenShift

Detalhes sobre instalção do openshift [Install OpenShift at your site](https://docs.openshift.com/container-platform/3.3/install_config/index.html).
- Utilização do Fabric8 [Get Started Guide](https://access.redhat.com/documentation/en/red-hat-jboss-middleware-for-openshift/3/single/red-hat-jboss-fuse-integration-services-20-for-openshift/)

Deploy com fabric8:

    mvn fabric8:deploy

Quando o exemplo é executado em OpenShift, você pode usar a ferramenta cliente OpenShift para inspecionar o status

Listando os pods em execução:

    oc get pods

Then find the name of the pod that runs this quickstart, and output the logs from the running pods with:

    oc logs <name of pod>



### Running via an S2I Application Template



Primeiro, importe os fluxos de imagem do Fuse:

    oc create -f https://raw.githubusercontent.com/jboss-fuse/application-templates/GA/fis-image-streams.json

Em seguida, crie o modelo de início rápido:

    oc create -f https://raw.githubusercontent.com/jboss-fuse/application-templates/GA/quickstarts/spring-boot-camel-template.json

Agora, ao usar o botão "Adicionar ao projeto" no console do OpenShift, você verá um modelo para este guia de início rápido.


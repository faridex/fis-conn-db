package org.mycompany;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

@Component
public class ProdutoRoute extends RouteBuilder {

	
	@Override
	public void configure() throws Exception {
		
		restConfiguration()
        .contextPath("/camel").apiContextPath("/api-doc")
            .apiProperty("api.title", "Camel REST API")
            .apiProperty("api.version", "1.0")
            .apiProperty("cors", "true")
            .apiContextRouteId("doc-api")
        .component("servlet")
        .bindingMode(RestBindingMode.json);
		
		rest().description("API de integracao com produtos")
		
			.get("/produto").description("End point que exibi os valores de produto")
			.produces(MediaType.APPLICATION_JSON_VALUE)
			.route().routeId("route-get-produto")
			.to("sql:select * from Produto"
		//			+ "?dataSource=dataSource"
					+ "?outputClass=org.mycompany.Produto")
			.log(":::Registro Retornado:::")
			.endRest()
			

	/*	.get("/produtoBB").description("End point que exibi os valores de produto")
		.produces(MediaType.APPLICATION_JSON_VALUE)
		.route().routeId("route-bb")
		.removeHeaders("CamelHttp*")
		.setHeader(Exchange.HTTP_METHOD, constant("GET"))
		.to("http://fis-conn-db-m01.apps.fort-2923.example.opentlc.com/camel/produto")
		.log(":::buscando da Fonte:::")
		.endRest()*/
		;
		
		
	}

}
